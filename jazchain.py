##################################################################
#
#
#
# File name: jazchain.py
# Author: Nehuen Ramirez
# Instagram: @nehuen.rch
# Date created: 2021-02-27
# Date last modified: 2021-03-05
# Python Version: 2.7
#
#
#
##################################################################

##################################################################
#
# Libraries
#

import hashlib
import sys
import random
import json

#
##################################################################


##################################################################
#
# Functions
#

def generateUsers (amount, randomNames, initialWealth):

	users = []
	names = []

	users.append (generateMinerUserProfile ())

	while len (names) < amount:

		randomInt = random.randint (0, len (randomNames) - 1)

		if randomNames [randomInt] not in names:

			names.append (randomNames [randomInt])


	for name in names:

		user = {}
		user ['name'] = name
		user ['hash'] = hashlib.md5 (name.encode ()).hexdigest ()
		user ['initial_balance'] = initialWealth
		user ['current_balance'] = initialWealth
		user ['transactions_count'] = 0

		users.append (user)


	return users


def generateMinerUserProfile ():

	user = {}
	user ['name'] = 'THIS COMPUTER'
	user ['hash'] = hashlib.md5 (user ['name'].encode ()).hexdigest ()
	user ['initial_balance'] = 0
	user ['current_balance'] = 0
	user ['transactions_count'] = 0

	return user


def transactionsCount (chain):

	transactionsCount = 0

	for block in chain:

		transactionsCount += block ['transactionsCount']


	return transactionsCount


def generateBlock (chain, PoW, prize):

	previousHash = generatePreviousHash (chain, PoW)


	if len (chain) > 0:
		print 'OK      proof of work successful after ' + str (chain [-1]['nonce']) + ' tries, block ' + str (len (jazchain)) + ' hash is ' + previousHash
		print 'OK      this computer (' + chain [-1]['transactions'][0]['destination'] + ') has been awarded with ' + str (prize) + ' units for mining block ' + str (len (jazchain))
		print ''
		 

	block = {}
	block ['transactions'] = []
	block ['previousHash'] = previousHash
	block ['merkleTree'] = False
	block ['rootHash'] = False
	block ['transactionsCount'] = 0
	block ['nonce'] = 0								# a nonce is a number used only once and with a very specified target

	chain.append (block)


	return chain


def generatePreviousHash (chain, PoW):

	previousHash = ''


	if len (chain) > 0:


		hashInput = chain [-1]['previousHash'] + chain [-1]['rootHash'] + str (chain [-1]['nonce'])

		previousHash = hashlib.md5 (hashInput.encode ()).hexdigest ()


		while previousHash [:PoW] != '0' * PoW:
		
			chain [len (chain) - 1]['nonce'] += 1

			hashInput = chain [-1]['previousHash'] + chain [-1]['rootHash'] + str (chain [-1]['nonce'])

			previousHash = hashlib.md5 (hashInput.encode ()).hexdigest ()


	return previousHash


def reclaimDiskSpace (chain, storationLength):

	if len (chain) > storationLength:

		chain [-storationLength]['transactions'] = False
		chain [-storationLength]['merkleTree'] = False

		print chain [-storationLength]


	return chain


def generateBlockPrize (users, prize):

	transaction = {}
	transaction ['origin'] = ''
	transaction ['destination'] = users [0]['hash']
	transaction ['value'] = prize

	users = updateBalance (users, transaction ['destination'], transaction ['value'])


	return users, transaction


def simulateTransaction (users, fee):

	origin, destination, miner, value, transaction, feeTransaction = generateTransaction (users, fee)

	if feeTransaction:

		users = updateBalance (users, origin, -value)
		users = updateBalance (users, destination, value - fee)
		users = updateBalance (users, miner, fee)

	else:

		users = updateBalance (users, origin, -value)
		users = updateBalance (users, destination, value)


	return users, transaction, feeTransaction


def generateTransaction (users, fee):

	originIndex = random.randint (0, len (users) - 1)

	destinationIndex = random.randint (1, len (users) - 1)

	transactionValue = min (random.randint (1, 10), users [originIndex]['current_balance']) + fee


	while originIndex == destinationIndex or users [originIndex]['current_balance'] - transactionValue < 0 or users [originIndex]['current_balance'] == 0:

		originIndex = random.randint (0, len (users) - 1)

		transactionValue = min (random.randint (1, 10), users [originIndex]['current_balance']) + fee


	origin = users [originIndex]['hash']

	destination = users [destinationIndex]['hash']

	miner = users [0]['hash']


	transaction = {}
	transaction ['origin'] = origin
	transaction ['destination'] = destination
	transaction ['value'] = transactionValue - fee


	feeTransaction = False

	if origin != miner:

		feeTransaction = {}
		feeTransaction ['origin'] = origin
		feeTransaction ['destination'] = miner
		feeTransaction ['value'] = fee


	return transaction ['origin'], transaction ['destination'], miner, transaction ['value'], transaction, feeTransaction


def updateBalance (users, user, value):

	for i in users:

		if i ['hash'] == user:

			i ['current_balance'] = i ['current_balance'] + value

			i ['transactions_count'] += 1


	return users


def storeTransaction (chain, transaction):

	chain [-1]['transactions'].append (transaction)

	chain [-1]['transactionsCount'] += 1


	return chain


def generateMerkleTree (transactions):
	
	merkleTree = []


	merkleTree.append ([])

	for transactionIndex in range (len (transactions) - 1):

			transactionHash = hashlib.md5 (str (transactions [transactionIndex]).encode ()).hexdigest ()

			merkleTree [0].append (transactionHash)

	
	while len (merkleTree [-1]) > 1:

		merkleTree.append ([])

		for i in range (len (merkleTree [-2]) - 1):

			rest = len (merkleTree [-2]) - 2 * i

			if rest <= 0:
				break

			elif rest == 1:
				merkleTree [-1].append (merkleTree [-2][2 * i])

			else:
				merkleTree [-1].append (hashlib.md5 (str (merkleTree [-2][2 * i] + merkleTree [-2][2 * i + 1]).encode ()).hexdigest ())


	return merkleTree


def integrityCheck (chain):

	chainCorrupted = False

	for blockIndex in range (1, len (chain) - 1):

		hashInput = chain [blockIndex - 1]['previousHash'] + chain [blockIndex - 1]['rootHash'] + str (chain [blockIndex - 1]['nonce'])

		if chain [blockIndex]['previousHash'] != hashlib.md5 (hashInput.encode ()).hexdigest ():

			chainCorrupted = True

	print ''

	if chainCorrupted:
		print 'ALERT   blockchain integrity check not passed (chain has been tampered with)'

	else:
		print 'OK      blockchain integrity check passed'


	return


def generateRandomManipulation (chain, BLOCK_transactionsPerBlock, performManipulation):

	print ''

	if performManipulation:

		randomBlockIndex = random.randint (0, len (chain) - 1)

		randomTransactionIndex = random.randint (1, BLOCK_transactionsPerBlock - 1)

		formerTransactionOrigin = chain [randomBlockIndex]['transactions'][randomTransactionIndex]['origin']
		formerTransactionDestination = chain [randomBlockIndex]['transactions'][randomTransactionIndex]['destination']
		formerTransactionValue = chain [randomBlockIndex]['transactions'][randomTransactionIndex]['value']
		
		chain [randomBlockIndex]['transactions'][randomTransactionIndex]['value'] = 1000

		print 'OK      random manipulation active'
		print 'OK      manipulation will be performed on block ' + str (randomBlockIndex + 1) + ', transaction ' + str (randomTransactionIndex + 1)
		print 'OK      former transaction details: ' + formerTransactionOrigin + ' sent ' + str (formerTransactionValue) + ' units to ' + formerTransactionDestination
		print 'OK      new transaction details: ' + formerTransactionOrigin + ' sent 1000 units to ' + formerTransactionDestination
		print 'OK      manipulation performed'

	else:

		print 'OK      manipulation not performed'


	return chain


			




'''

def recalculateBalance (chain, user):

	balance = 1000

	for i in chain:

		for ii in i ['transactions']:


			if ii ['origin'] == hashlib.md5 (user.encode ()).hexdigest ():

				balance -= ii ['value']


			if ii ['destination'] == hashlib.md5 (user.encode ()).hexdigest ():

				balance += ii ['value']

	return balance

def showResult (chain, users):

	print ('')
	print ('')

	transactionsCount = 0

	for i in users:

		currentBalance = recalculateBalance (chain, i ['name'])

		print ('Name: ' + i ['name'])
		print ('Balance (initial): ' + str (i ['initial_balance']))
		print ('Balance (final): ' + str (i ['balance']))
		print ('Balance (corrupted): ' + str (currentBalance))
		print ('Transactions count: ' + str (i ['transactions_count']))
		print ('')
		print ('')
		transactionsCount += i ['transactions_count']

	print ('Chain length: ' + str (len (chain)) + ' blocks')
	print ('')

	print ('Transactions per block: ' + str (len (chain [0]['transactions'])))
	print ('')

	print ('Transactions count: ' + str (transactionsCount / 2))
	print ('')

	print ('Blockchain corrupted: ' + str (not checkIntegrity (chain)))
	print ('')
	print ('')
	print ('')


	return

'''

#
##################################################################


##################################################################
#
# Parameters
#

CHAIN_transactions = 10000											# how many transactions will be simulated

BLOCK_transactionsPerBlock = 250									# how many transactions a block will store

BLOCK_miningPrize = 50												# every mined block will provide the miner (this computer) with a 1 unit prize

BLOCK_prizeHalvingRate = 15											# prize halving will take place every time a new block is created

BLOCK_storationLength = 5											# transaction details will be removed after 5 blocks have been created after their holding block

PoW_amountOfZeros = 4												# how many zeros on the block hash does the Proof of Work require

TRANSACTION_fee = 1													# the transaction fee will be charged to the sender

USERS_initialWealth = 0												# the initial wealth of every user is 1000 units

USERS_randomNames = ['Sophia', 'Liam', 'Olivia', 'Noah', 'Riley', 'Jackson', 'Emma', 'Aiden', 'Ava', 'Elijah', 'Isabella', 'Grayson', 'Aria', 'Lucas', 'Aaliyah', 'Oliver', 'Amelia', 'Caden', 'Mia', 'Mateo', 'Layla', 'Muhammad', 'Zoe', 'Mason', 'Camilla', 'Carter', 'Charlotte', 'Jayden', 'Eliana', 'Ethan', 'Mila', 'Sebastian', 'Everly', 'James', 'Luna', 'Michael', 'Avery', 'Benjamin', 'Evelyn', 'Logan', 'Harper', 'Leo', 'Lily', 'Luca', 'Ella', 'Alexander', 'Gianna', 'Levi', 'Chloe', 'Daniel', 'Adalyn', 'Josiah', 'Charlie', 'Henry', 'Isla', 'Jayce', 'Ellie', 'Julian', 'Leah', 'Jack', 'Nora', 'Ryan', 'Scarlett', 'Jacob', 'Maya', 'Asher', 'Abigail', 'Wyatt', 'Madison', 'William', 'Aubrey', 'Owen', 'Emily', 'Gabriel', 'Kinsley', 'Miles', 'Elena', 'Lincoln', 'Paisley', 'Ezra', 'Madelyn', 'Isaiah', 'Aurora', 'Luke', 'Peyton', 'Cameron', 'Nova', 'Caleb', 'Emilia', 'Isaac', 'Hannah', 'Carson', 'Sarah', 'Samuel', 'Ariana', 'Colton', 'Penelope', 'Maverick', 'Lila', 'Matthew']		# a list of random names

USERS_amount = min (random.randint (3, len (USERS_randomNames) - 1), 9)		# users that will make transactions

print ''
print ''
print 'OK      JazChain started'
print ''
print 'OK      parameters set'
print 'OK      proof of work: every block hash must start with ' + str (PoW_amountOfZeros) + ' zeros '
print 'OK      comission fee: ' + str (TRANSACTION_fee) + ' unit for every transaction performed'
print ''

#
##################################################################


##################################################################
#
# Users
#

users = generateUsers (USERS_amount, USERS_randomNames, USERS_initialWealth)		# a random set of users is generated. A random pair of users will be involved in every transaction

print 'OK      ' + str (len (users)) + ' random users generated'
print ''

#
##################################################################


##################################################################
#
# JazChain
#

jazchain = []														# let's start by creating the chain

blockCounter = 0

inCirculationUnits = 0

marketVolume = 0


while transactionsCount (jazchain) < CHAIN_transactions:			# loop repeated untill the previously defined amount of transactions is simulated



	jazchain = generateBlock (jazchain, PoW_amountOfZeros, BLOCK_miningPrize)

	jazchain = reclaimDiskSpace (jazchain, BLOCK_storationLength)



	users, transaction = generateBlockPrize (users, BLOCK_miningPrize)

	jazchain = storeTransaction (jazchain, transaction)



	marketVolume += jazchain [-1]['transactions'][-1]['value']

	inCirculationUnits += BLOCK_miningPrize



	while len (jazchain [-1]['transactions']) < BLOCK_transactionsPerBlock - 1:

		users, transaction, feeTransaction = simulateTransaction (users, TRANSACTION_fee)

		jazchain = storeTransaction (jazchain, transaction)

		marketVolume += jazchain [-1]['transactions'][-1]['value']


		if feeTransaction:
		
			jazchain = storeTransaction (jazchain, feeTransaction)

			marketVolume += jazchain [-1]['transactions'][-1]['value']



	jazchain [-1]['merkleTree'] = generateMerkleTree (jazchain [-1]['transactions'])

	jazchain [-1]['rootHash'] = jazchain [-1]['merkleTree'][-1][0]



	if blockCounter == BLOCK_prizeHalvingRate:
		
		BLOCK_miningPrize *= .5

		blockCounter = 0

	blockCounter += 1



	if len (jazchain) == 1:
		print 'OK      block ' + str (len (jazchain)) + ' created (genesis block)'

	else:
		print 'OK      block ' + str (len (jazchain)) + ' created'


	print 'OK      ' + str (len (jazchain [-1]['transactions'])) + ' transactions were simulated and stored in block ' + str (len (jazchain))

#
##################################################################


##################################################################
#
# Transaction Manipulation
#
#
#
# Al descomentar la linea 390, se genera una manipulacion sobre la informacion original almacenada en la blockchain
#
# El recipiente, en lugar de recibir un valor entre $0 y $10, estaria recibiendo $1000, por lo que seria sustancialmente mas rico, dejando al mismo tiempo sustancialmente mas pobre al usuario origen de la transaccion
#

performManipulation = False

#performManipulation = True

jazchain = generateRandomManipulation (jazchain, BLOCK_transactionsPerBlock, performManipulation)

#
##################################################################


##################################################################
#
# Integrity check
#

integrityCheck (jazchain)

#
#
##################################################################


print ''
print 'OK      total units in circulation: ' + str (inCirculationUnits)
print ''
print 'OK      market volume: ' + str (marketVolume) + ' units'
print ''
print ''



with open ('export.txt', 'w') as f:

	f.write (str (json.dumps (jazchain)))














